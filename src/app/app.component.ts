import {Component, ViewChild} from '@angular/core';
import * as d3 from 'd3';
import {D3OrgChartComponent} from './d3-org-chart/d3-org-chart.component';
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent {
	@ViewChild(D3OrgChartComponent, {static: false}) child: D3OrgChartComponent;
	isNodeClick = false;
	data = [
		{
			nodeId: '1',
			parentNodeId: null,
			width: 124,
			height: 124,
			x: '-59',
			borderWidth: 4,
			borderRadius: 66,
			borderColor: {
				red: 196,
				green: 205,
				blue: 213,
				alpha: 1,
			},
			backgroundColor: {
				red: 255,
				green: 255,
				blue: 255,
				alpha: 1,
			},
			nodeImage: {
				url: 'assets/images/horse1.jpg',
				width: 100,
				height: 100,
				centerTopDistance: 62,
				centerLeftDistance: 62,
				cornerShape: 'CIRCLE',
				shadow: false,
				borderWidth: 0,
				borderColor: {
					red: 255,
					green: 255,
					blue: 255,
					alpha: 1,
				},
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			// template: `<div>
			//                <div style="margin-left: 142px;
			//                             margin-top: 48px;
			//                             font-size: 34px;
			//                             font-weight:bold;
			//                             color:rgba(53,64,81,1)">
			//                                Horse
			//                 </div>

			//             </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 9,
			totalSubordinates: 812,
		},
		{
			nodeId: '2',
			parentNodeId: '1',
			width: 331,
			height: 139,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 198,
				green: 224,
				blue: 228,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeImage: {
				url: 'assets/images/horse7.jpg',
				width: 100,
				height: 100,
				centerTopDistance: 66,
				centerLeftDistance: 66,
				cornerShape: 'CIRCLE',
				shadow: false,
				borderWidth: 0,
				borderColor: {
					red: 19,
					green: 123,
					blue: 128,
					alpha: 1,
				},
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                       <div style="margin-left: 142px;
                                        margin-top: 48px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:rgba(53,64,81,1)">
	                                       Horse
	                        </div>
	                        
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 9,
			totalSubordinates: 812,
		},
		{
			nodeId: '3',
			parentNodeId: '1',
			width: 348,
			height: 147,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 198,
				green: 224,
				blue: 228,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeImage: {
				url: 'assets/images/horse3.jpg',
				width: 100,
				height: 100,
				centerTopDistance: 66,
				centerLeftDistance: 66,
				cornerShape: 'CIRCLE',
				shadow: false,
				borderWidth: 0,
				borderColor: {
					red: 19,
					green: 123,
					blue: 128,
					alpha: 1,
				},
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                        <div style="margin-left: 142px;
                                        margin-top: 48px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:rgba(53,64,81,1)">
	                                       Horse
	                        </div>
	                        
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 3,
			totalSubordinates: 413,
		},
		{
			nodeId: '4',
			parentNodeId: '2',
			width: 345,
			height: 140,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 198,
				green: 224,
				blue: 228,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeImage: {
				url: 'assets/images/horse4.jpg',
				width: 100,
				height: 100,
				centerTopDistance: 66,
				centerLeftDistance: 66,
				cornerShape: 'CIRCLE',
				shadow: false,
				borderWidth: 0,
				borderColor: {
					red: 19,
					green: 123,
					blue: 128,
					alpha: 1,
				},
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                      <div style="margin-left: 142px;
                                        margin-top: 48px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:rgba(53,64,81,1)">
	                                       Horse
	                        </div>
	                        
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 3,
			totalSubordinates: 142,
		},
		{
			nodeId: '5',
			parentNodeId: '2',
			width: 311,
			height: 134,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 198,
				green: 224,
				blue: 228,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeImage: {
				url: 'assets/images/horse5.jpg',
				width: 100,
				height: 100,
				centerTopDistance: 66,
				centerLeftDistance: 66,
				cornerShape: 'CIRCLE',
				shadow: false,
				borderWidth: 0,
				borderColor: {
					red: 19,
					green: 123,
					blue: 128,
					alpha: 1,
				},
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                        <div style="margin-left: 142px;
                                        margin-top: 48px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:rgba(53,64,81,1)">
	                                       Horse
	                        </div>
	                        
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 3,
			totalSubordinates: 144,
		},
		{
			nodeId: '6',
			parentNodeId: '3',
			width: 321,
			height: 150,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 198,
				green: 224,
				blue: 228,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeImage: {
				url: 'assets/images/horse6.jpg',
				width: 100,
				height: 100,
				centerTopDistance: 66,
				centerLeftDistance: 66,
				cornerShape: 'CIRCLE',
				shadow: false,
				borderWidth: 0,
				borderColor: {
					red: 19,
					green: 123,
					blue: 128,
					alpha: 1,
				},
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                        <div style="margin-left: 142px;
                                        margin-top: 48px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:rgba(53,64,81,1)">
	                                       Horse
	                        </div>
	                        
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 9,
			totalSubordinates: 429,
		},
		{
			nodeId: '7',
			parentNodeId: '4',
			width: 329,
			height: 147,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 145,
				green: 158,
				blue: 171,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                        <div style="margin-left: 80px;
                                        margin-top: 52px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:#637381">
	                                       Add Name
	                        </div>
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 9,
			totalSubordinates: 159,
		},
		// {
		// 	nodeId: '8',
		// 	parentNodeId: '4',
		// 	width: 329,
		// 	height: 147,
		// 	borderWidth: 1,
		// 	borderRadius: 5,
		// 	borderColor: {
		// 		red: 145,
		// 		green: 158,
		// 		blue: 171,
		// 		alpha: 1,
		// 	},
		// 	backgroundColor: {
		// 		red: 227,
		// 		green: 240,
		// 		blue: 241,
		// 		alpha: 1,
		// 	},
		// 	nodeIcon: {
		// 		icon: 'https://to.ly/1yZnX',
		// 		size: 30,
		// 	},
		// 	template: `<div>
		//                     <div style="margin-left: 80px;
		//                                 margin-top: 52px;
		//                                 font-size: 34px;
		//                                 font-weight:bold;
		//                                 color:#637381">
		//                                    Add Name
		//                     </div>
		//                 </div>`,
		// 	connectorLineColor: {
		// 		red: 59,
		// 		green: 60,
		// 		blue: 63,
		// 		alpha: 1,
		// 	},
		// 	connectorLineWidth: 5,
		// 	dashArray: '',
		// 	expanded: true,
		// 	directSubordinates: 9,
		// 	totalSubordinates: 159,
		// },
		{
			nodeId: '9',
			parentNodeId: '3',
			width: 329,
			height: 147,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 145,
				green: 158,
				blue: 171,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                        <div style="margin-left: 80px;
                                        margin-top: 52px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:#637381">
	                                       Add Name
	                        </div>
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 9,
			totalSubordinates: 159,
		},
		{
			nodeId: '10',
			parentNodeId: '5',
			width: 329,
			height: 147,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 145,
				green: 158,
				blue: 171,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                        <div style="margin-left: 80px;
                                        margin-top: 52px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:#637381">
	                                       Add Name
	                        </div>
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 9,
			totalSubordinates: 159,
		},
		{
			nodeId: '11',
			parentNodeId: '5',
			width: 329,
			height: 147,
			borderWidth: 1,
			borderRadius: 5,
			borderColor: {
				red: 145,
				green: 158,
				blue: 171,
				alpha: 1,
			},
			backgroundColor: {
				red: 227,
				green: 240,
				blue: 241,
				alpha: 1,
			},
			nodeIcon: {
				icon: 'https://to.ly/1yZnX',
				size: 30,
			},
			template: `<div>
	                        <div style="margin-left: 80px;
                                        margin-top: 52px;
                                        font-size: 34px;
                                        font-weight:bold;
                                        color:#637381">
	                                       Add Name
	                        </div>
	                    </div>`,
			connectorLineColor: {
				red: 59,
				green: 60,
				blue: 63,
				alpha: 1,
			},
			connectorLineWidth: 5,
			dashArray: '',
			expanded: true,
			directSubordinates: 9,
			totalSubordinates: 159,
		},
		// {
		// 	nodeId: '12',
		// 	parentNodeId: '6',
		// 	width: 329,
		// 	height: 147,
		// 	borderWidth: 1,
		// 	borderRadius: 5,
		// 	borderColor: {
		// 		red: 145,
		// 		green: 158,
		// 		blue: 171,
		// 		alpha: 1,
		// 	},
		// 	backgroundColor: {
		// 		red: 227,
		// 		green: 240,
		// 		blue: 241,
		// 		alpha: 1,
		// 	},
		// 	nodeIcon: {
		// 		icon: 'https://to.ly/1yZnX',
		// 		size: 30,
		// 	},
		// 	template: `<div>
		//                     <div style="margin-left: 80px;
		//                                 margin-top: 52px;
		//                                 font-size: 34px;
		//                                 font-weight:bold;
		//                                 color:#637381">
		//                                    Add Name
		//                     </div>
		//                 </div>`,
		// 	connectorLineColor: {
		// 		red: 59,
		// 		green: 60,
		// 		blue: 63,
		// 		alpha: 1,
		// 	},
		// 	connectorLineWidth: 5,
		// 	dashArray: '',
		// 	expanded: true,
		// 	directSubordinates: 9,
		// 	totalSubordinates: 159,
		// },
		// {
		// 	nodeId: '13',
		// 	parentNodeId: '6',
		// 	width: 329,
		// 	height: 147,
		// 	borderWidth: 1,
		// 	borderRadius: 5,
		// 	borderColor: {
		// 		red: 145,
		// 		green: 158,
		// 		blue: 171,
		// 		alpha: 1,
		// 	},
		// 	backgroundColor: {
		// 		red: 227,
		// 		green: 240,
		// 		blue: 241,
		// 		alpha: 1,
		// 	},
		// 	nodeIcon: {
		// 		icon: 'https://to.ly/1yZnX',
		// 		size: 30,
		// 	},
		// 	template: `<div>
		//                     <div style="margin-left: 80px;
		//                                 margin-top: 52px;
		//                                 font-size: 34px;
		//                                 font-weight:bold;
		//                                 color:#637381">
		//                                    Add Name
		//                     </div>
		//                 </div>`,
		// 	connectorLineColor: {
		// 		red: 59,
		// 		green: 60,
		// 		blue: 63,
		// 		alpha: 1,
		// 	},
		// 	connectorLineWidth: 5,
		// 	dashArray: '',
		// 	expanded: true,
		// 	directSubordinates: 9,
		// 	totalSubordinates: 159,
		// },
		// {
		// 	nodeId: '14',
		// 	parentNodeId: '9',
		// 	width: 329,
		// 	height: 147,
		// 	borderWidth: 1,
		// 	borderRadius: 5,
		// 	borderColor: {
		// 		red: 145,
		// 		green: 158,
		// 		blue: 171,
		// 		alpha: 1,
		// 	},
		// 	backgroundColor: {
		// 		red: 227,
		// 		green: 240,
		// 		blue: 241,
		// 		alpha: 1,
		// 	},
		// 	nodeIcon: {
		// 		icon: 'https://to.ly/1yZnX',
		// 		size: 30,
		// 	},
		// 	template: `<div>
		//                     <div style="margin-left: 80px;
		//                                 margin-top: 52px;
		//                                 font-size: 34px;
		//                                 font-weight:bold;
		//                                 color:#637381">
		//                                    Add Name
		//                     </div>
		//                 </div>`,
		// 	connectorLineColor: {
		// 		red: 59,
		// 		green: 60,
		// 		blue: 63,
		// 		alpha: 1,
		// 	},
		// 	connectorLineWidth: 5,
		// 	dashArray: '',
		// 	expanded: true,
		// 	directSubordinates: 9,
		// 	totalSubordinates: 159,
		// },
		// {
		// 	nodeId: '15',
		// 	parentNodeId: '9',
		// 	width: 329,
		// 	height: 147,
		// 	borderWidth: 1,
		// 	borderRadius: 5,
		// 	borderColor: {
		// 		red: 145,
		// 		green: 158,
		// 		blue: 171,
		// 		alpha: 1,
		// 	},
		// 	backgroundColor: {
		// 		red: 227,
		// 		green: 240,
		// 		blue: 241,
		// 		alpha: 1,
		// 	},
		// 	nodeIcon: {
		// 		icon: 'https://to.ly/1yZnX',
		// 		size: 30,
		// 	},
		// 	template: `<div>
		//                     <div style="margin-left: 80px;
		//                                 margin-top: 52px;
		//                                 font-size: 34px;
		//                                 font-weight:bold;
		//                                 color:#637381">
		//                                    Add Name
		//                     </div>
		//                 </div>`,
		// 	connectorLineColor: {
		// 		red: 59,
		// 		green: 60,
		// 		blue: 63,
		// 		alpha: 1,
		// 	},
		// 	connectorLineWidth: 5,
		// 	dashArray: '',
		// 	expanded: true,
		// 	directSubordinates: 9,
		// 	totalSubordinates: 159,
		// },
	];
	// data = [
	// 	{
	// 		nodeId: 'O-1',
	// 		parentNodeId: null,
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 0,
	// 			centerLeftDistance: 0,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 1,
	// 			},
	// 		},
	// 		// nodeImage: {
	// 		// 	url: 'assets/images/horse5.jpg',
	// 		// 	width: 100,
	// 		// 	height: 100,
	// 		// 	centerTopDistance: 66,
	// 		// 	centerLeftDistance: 66,
	// 		// 	cornerShape: 'CIRCLE',
	// 		// 	shadow: false,
	// 		// 	borderWidth: 0,
	// 		// 	borderColor: {
	// 		// 		red: 19,
	// 		// 		green: 123,
	// 		// 		blue: 128,
	// 		// 		alpha: 0,
	// 		// 	},
	// 		// },
	// 		template: `<div style="color:#2A2A2A;
	//                         height:160px;
	//                         margin-top:-30px;
	//                         background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">
	//                             <div style="margin-left:147px;
	//                                         margin-top:30px;
	//                                         padding-top:20px;
	//                                         font-size:20px;
	//                                         font-weight:bold;">
	//                                             Ian Devling
	//                             </div>
	//                             <div style="margin-left:147px;
	//                                         margin-top:3px;
	//                                         font-size:16px;">
	//                                             Cheaf Executive Officer
	//                             </div>
	//                             <div style="margin-left:147px;
	//                                         margin-top:3px;
	//                                         font-size:14px;">
	//                                             Business first
	//                             </div>
	//                             <div style="margin-left:270px;
	//                                         margin-top:15px;
	//                                         font-size:13px;
	//                                         position:absolute;
	//                                         bottom:5px;">
	//                                             <div>
	//                                                 CTO office
	//                                             </div>
	//                                             <div style="margin-top:5px">
	//                                                 Corporate
	//                                             </div>
	//                             </div>
	//                         </div>`,
	// connectorLineColor: {
	// 	red: 59,
	// 	green: 60,
	// 	blue: 63,
	// 	alpha: 1,
	// },
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 4,
	// 		totalSubordinates: 1515,
	// 	},
	// 	{
	// 		nodeId: 'O-2',
	// 		parentNodeId: 'O-1',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         ">Davolio Nancy </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">CTO  </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">Business one</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 9,
	// 		totalSubordinates: 812,
	// 	},
	// 	{
	// 		nodeId: 'O-3',
	// 		parentNodeId: 'O-1',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         "> Leverling Janet </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">CTO  </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">Business two </div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 3,
	// 		totalSubordinates: 413,
	// 	},
	// 	{
	// 		nodeId: 'O-4',
	// 		parentNodeId: 'O-1',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         "> Leverling Janet </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">CTO  </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">Business three</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 3,
	// 		totalSubordinates: 142,
	// 	},
	// 	{
	// 		nodeId: 'O-5',
	// 		parentNodeId: 'O-1',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         "> Leverling Janet </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">CTO  </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">Business four </div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 3,
	// 		totalSubordinates: 144,
	// 	},
	// 	{
	// 		nodeId: 'O-6',
	// 		parentNodeId: 'O-2',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         "> Leverling Janet </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">CTO  </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         "> Finance Department</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 9,
	// 		totalSubordinates: 429,
	// 	},
	// 	{
	// 		nodeId: 'O-7',
	// 		parentNodeId: 'O-2',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         ">Fuller Andrew </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">Linear Manager </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">Marketing Department</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 9,
	// 		totalSubordinates: 159,
	// 	},
	// 	{
	// 		nodeId: 'O-8',
	// 		parentNodeId: 'O-2',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         ">Peacock Margaret </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">CEO </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">HR Department</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 9,
	// 		totalSubordinates: 24,
	// 	},
	// 	{
	// 		nodeId: 'O-9',
	// 		parentNodeId: 'O-2',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         ">Buchanan Steven </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">Head of direction </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">Facility Management</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 9,
	// 		totalSubordinates: 136,
	// 	},
	// 	{
	// 		nodeId: 'O-10',
	// 		parentNodeId: 'O-2',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url:
	// 				'https://raw.githubusercontent.com/bumbeishvili/Assets/master/Projects/D3/Organization%20Chart/general.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         ">Suyama Michael </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">Senior sales manager </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">IT Consulting</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 0,
	// 		totalSubordinates: 0,
	// 	},
	// 	{
	// 		nodeId: 'O-11',
	// 		parentNodeId: 'O-2',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         ">King Robert </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">Senior Sales Manager </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">Customer Service</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 2,
	// 		totalSubordinates: 26,
	// 	},
	// 	{
	// 		nodeId: 'O-12',
	// 		parentNodeId: 'O-2',
	// 		width: 350,
	// 		height: 160,
	// 		borderWidth: 0.6,
	// 		borderRadius: 0,
	// 		borderColor: {
	// 			red: 15,
	// 			green: 140,
	// 			blue: 121,
	// 			alpha: 0.7,
	// 		},
	// 		backgroundColor: {
	// 			red: 247,
	// 			green: 247,
	// 			blue: 249,
	// 			alpha: 0.4,
	// 		},
	// 		nodeIcon: {
	// 			size: 50,
	// 		},
	// 		nodeImage: {
	// 			url: 'assets/images/horse5.jpg',
	// 			width: 100,
	// 			height: 100,
	// 			centerTopDistance: 66,
	// 			centerLeftDistance: 66,
	// 			cornerShape: 'CIRCLE',
	// 			shadow: false,
	// 			borderWidth: 0,
	// 			borderColor: {
	// 				red: 19,
	// 				green: 123,
	// 				blue: 128,
	// 				alpha: 0,
	// 			},
	// 		},
	// 		template:
	// 			'<div style="color:#2A2A2A;height:160px;margin-top:-30px;background-image: linear-gradient(to right , #FFFFFF, #ECEDF0);">\n                  <div style="margin-left:147px;\n                              margin-top:30px;\n                              padding-top:20px;\n                              font-size:20px;\n                              font-weight:bold;\n                         ">West Adam </div>\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:16px;\n                         ">CTO </div>\n\n                 <div style="margin-left:147px;\n                              margin-top:3px;\n                              font-size:14px;\n                         ">Delivery Department</div>\n\n                 <div style="margin-left:270px;\n                             margin-top:15px;\n                             font-size:13px;\n                             position:absolute;\n                             bottom:5px;\n                            ">\n                      <div>CEO office</div>\n                      <div style="margin-top:5px">Corporate</div>\n                 </div>\n              </div>',
	// 		connectorLineColor: {
	// 			red: 59,
	// 			green: 60,
	// 			blue: 63,
	// 			alpha: 1,
	// 		},
	// 		connectorLineWidth: 2,
	// 		dashArray: '',
	// 		expanded: false,
	// 		directSubordinates: 0,
	// 		totalSubordinates: 0,
	// 	},
	// ];

	ngOnInit() {}
	nodeClicked(nodeId) {
		let duplicateData = this.data;
		if (!this.isNodeClick) {
			this.isNodeClick = true;
			duplicateData = this.data.filter((obj) => {
				if (obj.nodeId == nodeId) {
					// obj.template = `<input type="text" style="width: 184px;
					//                                             height: 49px;
					//                                             font-size: 18px;
					//                                             background-color: #F6F6F6;
					//                                             border: 1px solid #919EAB;
					//                                             border-radius: 5px;
					//                                             position: relative;
					//                                             top: 40px;
					//                                             left: 131px;" (onkeydown)="inputBoxValue(this)"/>`;
					obj.template = `<label>
                                    <input list="browsers" name="myBrowser" 
                                                            style="width: 323px;
                                                            height: 135px;
                                                            font-size: 32px;
                                                            background-color: #F6F6F6;
                                                            border: 1px solid #919EAB;
                                                            border-radius: 5px;
                                                            position: relative;
                                                            top: 0px;
                                                            left: 0px;
                                                            border-bottom: 8px solid #2DBAFF;"/>
                                </label>
                                <datalist id="browsers">
                                    <option value="Black jack">
                                    <option value="Trigger">
                                    <option value="Gene">
                                    <option value="Balaq">
                                    <option value="Banchee">
                                    <option value="Batman">
                                    <option value="Bolt">
                                    <option value="Buddy">
                                    <option value="Ransome">
                                </datalist>`;
				}
			});
			this.child.updateChart();
		}
	}
	inputBoxValue(value) {}
}
