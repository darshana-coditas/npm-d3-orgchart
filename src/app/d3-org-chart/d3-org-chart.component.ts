import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import TreeChart from 'd3-org-chart';
@Component({
	selector: 'app-d3-org-chart',
	templateUrl: './d3-org-chart.component.html',
	styleUrls: ['./d3-org-chart.component.css'],
})
export class D3OrgChartComponent implements OnInit {
	@ViewChild('chartContainer', {static: false}) chartContainer: ElementRef;
	@Input() data: any[];
	@Output() nodeClickedEvent = new EventEmitter();
	chart;

	constructor() {}

	ngOnInit() {
		// if (!this.chart) {
		// 	this.chart = new TreeChart();
		// }
		// this.updateChart();
	}

	ngAfterViewInit() {
		if (!this.chart) {
			this.chart = new TreeChart();
		}
		this.updateChart();
	}
	ngOnChanges() {
		// this.updateChart();
	}
	updateChart() {
		if (!this.data) {
			return;
		}
		if (!this.chart) {
			return;
		}
		this.chart
			.container(this.chartContainer.nativeElement)
			.data(this.data)
			.svgWidth(500)
			.initialZoom(0.4)
			.onNodeClick((d) => this.abc(d))
			// .layout('h')
			.render();
	}
	abc(a) {
		console.log('result=', a);
		this.nodeClickedEvent.emit(a);
		// this.updateChart();
	}
	inputBoxValue(value) {
		console.log(value);
	}
}
